# es6-demo
Belajar setup babel untuk mengcompile es6. <br>
Babel merupakan plugin Javascript compiler yang bisa mengubah kode yang ditulis menggunakan ES6 menjadi kode yang bisa berjalan pada browser yang tidak atau belum mendukung ES6.<br><br>

Source code: 
```
https://github.com/jstrip/es6-demo.git
```

Silahkan download source code dari repository tersebut dan ikuti petunjuknya!